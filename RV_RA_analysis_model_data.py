# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 17:17:35 2021

@author: suanov
"""

# ================================================================================================
# Libs import
# ================================================================================================
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import constants
# import _00_aux_func as af
# from scipy.fftpack import fft

# ================================================================================================


print("\n-------------------        RAYDAYAN        -------------------\n")
# non-interactive backend
# mpl.use('agg')
mpl.use('Qt5Agg')

# ================================================================================================
####    Principal Constants
# ================================================================================================
pi = np.pi
gr = pi/180.0
c = constants.speed_of_light   # (м/с)
i = 1j
# ================================================================================================
f0 = 77.0e9
smp_num = 256
fd = 18.75e6
td = 1/fd
S  = 25e12
ta = 5e-6              # ADC start time
ts = smp_num / fd      # ADC sampling time
fb = S * ts            # bandwidth
flo = f0 + ta*S        # low frequency
fhi = f0 + (ta+ts)*S   # high frequency
fc = (flo + fhi)/2.0   # central frequency
t_interframe = 1e-3    # intersubframe time

ti_azm = 5.0e-6            
tr_azm = 20.0e-6          
tp_azm = ti_azm + tr_azm          

ti_elv = 5.0e-6            
tr_elv = 20.0e-6          
tp_elv = ti_elv + tr_elv      

ti_vel = 5.0e-6            
tr_vel = 20.0e-6          
tp_vel = ti_vel + tr_vel       

####    general physical and data parameters
sbfrs  = 3             # number of subframes
lmb    = c/fc          # wavelength, m
k      = 2*pi/lmb      # wavenumber, rad/m
smp_w  = 2             # bytes per a sample
d      = lmb/2.0       # antenna spacing, m
tx_azm = 4             # number of tx for azm MIMO subframe
tx_elv = 5             # number of tx for elv MIMO subframe
tx_vel = 4             # number of tx for vel subframe

pls_num_az = 59
pls_num_el = 55
pls_num_vf = 63


# coordinates
# x-axis: along azm antennas array (from ugly to bad)
# y-axis: along normal from radar to scene
# z-axis: along elv antennas array (from tx to rx)

# Tx phase centers
tx_x = d*np.array([33, 25, 21, 13,  9,  7, -7, -9, -11, -15, -19, -23])
tx_z = d*np.array([ 0,  0,  0,  0, -2, -1, -3, -4,   0,   0,   0,   0])

# Rx phase centers Master first antena is the array center
rx_x = d*np.array([32,  31,  30,  29, 16, 15, 14, 13, 0, -1, -2, -3, -16, -17, -18, -19])
rx_z = d*np.array([8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8])

rx_bad_ind    = np.array([ 0,  1,  2,  3])        # Bad chip Rx's indexes
rx_good_ind   = np.array([ 4,  5,  6,  7])        # Good chip Rx's indexes
rx_master_ind = np.array([ 8,  9, 10, 11])        # Master chip Rx's indexes
rx_ugly_ind   = np.array([12, 13, 14, 15])        # Ugly chip Rx's indexes

tx_azm_ind = np.array([  8,  9, 10, 11])          # Tx for azm subframe
tx_elv_ind = np.array([ 11,  5,  4,  6,  7])      # Tx for elv subframe
tx_vel_ind = np.array([  8,  9, 10, 11])                       # Tx for vel subframe

rx_phys = 4                                       # Rx number for one AWR

#=================================================================================================
####    scene parameters
#=================================================================================================
# radar movement
v_host =  np.array([0.0, 0.0, 0.0])     # m/s (x, y, z - components respectively)

# wall scene model
# dr = 1         # scattering centres spacing, m
# left_wall = np.zeros((20, 3), float)
# for ref in range (0, 20):
#     left_wall[ref, 0] = -15                        #  x
#     left_wall[ref, 1] = 25 - ref*dr                #  y
#     left_wall[ref, 2] =  0                         #  z

# left_corner = np.zeros((20, 3), float)
# for ref in range (0, 10):
#     left_corner[ref, 0] = -20 + (ref + 1)*dr      #  x
#     left_corner[ref, 1] = 30                      #  y
#     left_corner[ref, 2] =  0                      #  z
# for ref in range (10, 20):
#     left_corner[ref, 0] = -10                     #  x
#     left_corner[ref, 1] =  30 + (ref - 9)*dr      #  y
#     left_corner[ref, 2] =  0                      #  z

# far_wall = np.zeros((30, 3), float)
# for ref in range (0, 30):
#     far_wall[ref, 0] = -15 + (ref + 0.5)*dr        #  x
#     far_wall[ref, 1] = 35                          #  y
#     far_wall[ref, 2] =  0                          #  z

# right_wall = np.zeros((52, 3), float)
# for ref in range (0, 21):
#     right_wall[ref, 0] = 15                       #  x
#     right_wall[ref, 1] = 5 + ref*dr               #  y
#     right_wall[ref, 2] = 0                        #  z
# for ref in range (21, 52):
#     right_wall[ref, 0] = 15                       #  x
#     right_wall[ref, 1] = 24 + (ref - 21)*dr       #  y
#     right_wall[ref, 2] = 0                        #  z

# scene = np.concatenate((left_wall, left_corner, far_wall, right_wall), axis = 0)
# # scene = np.concatenate((left_wall, far_wall), axis = 0)
# #scene = far_wall
# tgt_num = len(scene)

# point targets scene model
# tgt_num = 5
# scene   = np.zeros((tgt_num, 3), float)
# v_scene = np.zeros((tgt_num, 3), float)

# scene[0, 0] = 17.32;      v_scene[0, 0] = 0;
# scene[0, 1] = 30.0;       v_scene[0, 1] = 0;
# scene[0, 2] = 0;          v_scene[0, 2] = 0;

# scene[1, 0] = -17.32;     v_scene[1, 0] = 0;
# scene[1, 1] = 30;         v_scene[1, 1] = 0;
# scene[1, 2] = 0;          v_scene[1, 2] = 0;

# scene[2, 0] = 0;          v_scene[2, 0] = 0;
# scene[2, 1] = 20;         v_scene[2, 1] = 0;
# scene[2, 2] = 0;          v_scene[2, 2] = 0;

# scene[3, 0] = -5;         v_scene[3, 0] = 0;
# scene[3, 1] = 25;         v_scene[3, 1] = 0;
# scene[3, 2] = 0;          v_scene[3, 2] = 0;

# scene[4, 0] = 5;          v_scene[4, 0] = 0;
# scene[4, 1] = 25;         v_scene[4, 1] = 0;
# scene[4, 2] = 0;          v_scene[4, 2] = 0;


tgt_num = 3
scene   = np.zeros((tgt_num, 3), float)
v_scene = np.zeros((tgt_num, 3), float)
scene[0, 0] =  -5.0;      v_scene[0, 0] =   0.0;
scene[0, 1] =  14.0;      v_scene[0, 1] =   5.0;
scene[0, 2] =  0.0;       v_scene[0, 2] =   0.0;

scene[1, 0] =  -5.0;      v_scene[1, 0] =   0.0;
scene[1, 1] =  14.0;      v_scene[1, 1] =  100.0;
scene[1, 2] =  1.0;       v_scene[1, 2] =   0.0;

scene[2, 0] =  18.0;      v_scene[2, 0] =   0.0;
scene[2, 1] =  7.0;       v_scene[2, 1] =   0.0;
scene[2, 2] =  2.2;       v_scene[2, 2] =   0.0;


#=================================================================================================
# Model data calculation
#=================================================================================================
data_dst = open('RV_RA_model_data_3_obj_noised.bin', 'wb')

A = 750000      # targets RCS factor
zz = []
tt = []
Xtb_azm = np.zeros((pls_num_az, tx_azm, rx_phys, smp_num*2), float)
Xtg_azm = np.zeros((pls_num_az, tx_azm, rx_phys, smp_num*2), float)
Xtm_azm = np.zeros((pls_num_az, tx_azm, rx_phys, smp_num*2), float)
Xtu_azm = np.zeros((pls_num_az, tx_azm, rx_phys, smp_num*2), float)

# azm subframe
print('azm subframe')
for pls_ref in range(0, pls_num_az):
    print('pls = ', pls_ref)
    for tx_ref in range (0, tx_azm):
        chip_tx_ref = tx_azm_ind[tx_ref]
        x_tx = tx_x[chip_tx_ref]
        z_tx = tx_z[chip_tx_ref]  # tx_y[chip_tx_ref] = 0 anywhere
        t_curr = (pls_ref*tx_azm + tx_ref)*tp_azm # + ta
        for smp_ref in range(0, smp_num):
            t_smp = smp_ref*td

            # bad chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_bad_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtb_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtb_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag
            zz.append(z)
            # good chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_good_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtg_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtg_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            # master chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_master_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtm_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtm_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            # ugly chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_ugly_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtu_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtu_azm[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            t_curr += td
            tt.append(t_curr)

# # elv subframe
print('elv subframe')
Xtb_elv = np.zeros((pls_num_el, tx_elv, rx_phys, smp_num*2), float)
Xtg_elv = np.zeros((pls_num_el, tx_elv, rx_phys, smp_num*2), float)
Xtm_elv = np.zeros((pls_num_el, tx_elv, rx_phys, smp_num*2), float)
Xtu_elv = np.zeros((pls_num_el, tx_elv, rx_phys, smp_num*2), float)
for pls_ref in range(0, pls_num_el):
    print('pls = ', pls_ref)
    for tx_ref in range (0, tx_elv):
        chip_tx_ref = tx_elv_ind[tx_ref]
        x_tx = tx_x[chip_tx_ref]
        z_tx = tx_z[chip_tx_ref]  # tx_y[chip_tx_ref] = 0 anywhere
        t_curr = t_interframe + pls_num_az*tx_azm*tp_azm + (pls_ref*tx_elv + tx_ref)*tp_elv    # + inter subframe delay
        for smp_ref in range(0, smp_num):
            t_smp = smp_ref*td

            # bad chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_bad_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtb_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtb_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag
            zz.append(z)           
            # good chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_good_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtg_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtg_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            # master chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_master_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtm_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtm_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            # ugly chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_ugly_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0

                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)

                Xtu_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtu_elv[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            t_curr += td
            tt.append(t_curr)

# vel subframe
print('vel subframe')
Xtb_vel = np.zeros((pls_num_vf, tx_vel, rx_phys, smp_num*2), float)
Xtg_vel = np.zeros((pls_num_vf, tx_vel, rx_phys, smp_num*2), float)
Xtm_vel = np.zeros((pls_num_vf, tx_vel, rx_phys, smp_num*2), float)
Xtu_vel = np.zeros((pls_num_vf, tx_vel, rx_phys, smp_num*2), float)
for pls_ref in range(0, pls_num_vf):
    print('pls = ', pls_ref)
    for tx_ref in range (0, tx_vel):
        chip_tx_ref = tx_vel_ind[tx_ref]
        x_tx = tx_x[chip_tx_ref]
        z_tx = tx_z[chip_tx_ref]  # tx_y[chip_tx_ref] = 0 anywhere
        t_curr = 2*t_interframe + pls_num_az*tx_azm*tp_azm + pls_num_el*tx_elv*tp_elv + (pls_ref*tx_vel + tx_ref)*tp_vel  # + 2 inter subframe delay
        for smp_ref in range(0, smp_num):
            t_smp = smp_ref*td
            
            # bad chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_bad_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0
                
                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_propp
                    sample_value += (A/R**2)*np.exp(i*sig_phase)
                                    
                Xtb_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtb_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag
            zz.append(z)           
            # good chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_good_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0
                
                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)
                                    
                Xtg_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtg_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag
                
            # master chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_master_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0
                
                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)
                                    
                Xtm_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtm_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            # ugly chip data
            for rx_ref in range(0, rx_phys):
                chip_rx_ref = rx_ugly_ind[rx_ref]
                x_rx = rx_x[chip_rx_ref]
                z_rx = rx_z[chip_rx_ref]   # rx_y[tx_ref] = 0 anywhere
                sample_value = 0.0 + i*0.0
                
                # scene scatered signal
                for tgt_ref in range (0, tgt_num):
                    x = scene[tgt_ref, 0] - v_host[0]*t_curr + v_scene[tgt_ref, 0]*t_curr
                    y = scene[tgt_ref, 1] - v_host[1]*t_curr + v_scene[tgt_ref, 1]*t_curr
                    z = scene[tgt_ref, 2] - v_host[2]*t_curr + v_scene[tgt_ref, 2]*t_curr

                    R_tx_obj = np.sqrt((x-x_tx)**2 + y**2 + (z-z_tx)**2)
                    R_obj_rx = np.sqrt((x-x_rx)**2 + y**2 + (z-z_rx)**2)
                    R = R_tx_obj + R_obj_rx
                    # propagation time
                    t_prop = R/c
                    # IF
                    f_dif  = S*t_prop
                    # sig_phase = -k*R + 2*pi*f_dif*t_smp
                    sig_phase = k*R + 2*pi*f_dif*t_smp # - pi*f_dif*t_prop
                    sample_value += (A/R**2)*np.exp(i*sig_phase)
                                    
                Xtu_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref]   = sample_value.real
                Xtu_vel[pls_ref, tx_ref, rx_ref, 2*smp_ref+1] = sample_value.imag

            t_curr += td
            tt.append(t_curr)

Xtb = np.concatenate((Xtb_azm.flatten(), Xtb_elv.flatten(), Xtb_vel.flatten()), axis=0)
Xtg = np.concatenate((Xtg_azm.flatten(), Xtg_elv.flatten(), Xtg_vel.flatten()), axis=0)
Xtm = np.concatenate((Xtm_azm.flatten(), Xtm_elv.flatten(), Xtm_vel.flatten()), axis=0)
Xtu = np.concatenate((Xtu_azm.flatten(), Xtu_elv.flatten(), Xtu_vel.flatten()), axis=0)
                                          
Xt = np.concatenate((Xtb, Xtg, Xtm, Xtu), axis=0)
Xt = Xt / max(Xt)

Xt_int = np.zeros(len(Xt), np.int16)

noise_std = 100000                 # noise std
max_sample_value_int = 1000      # maximum sample value
 
ref = 0
for val in Xt:
    Xt_int[ref]  = np.int16(np.round(val*max_sample_value_int))
    Xt_int[ref] += np.int16(-0.5*noise_std + noise_std*np.random.normal(0.0, 1))      # noise mix in
    ref += 1

Xt_int.tofile(data_dst)
data_dst.close()


plt.figure().patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
plt.plot(Xt_int[0:512:2], 'b-')
plt.plot(Xt_int[1:512:2], 'r--')
plt.grid(color='g', linestyle='--', linewidth=1)

# plt.figure().patch.set_facecolor('xkcd:pale green') # 'xkcd:pale green'  # xkcd:mauve
# plt.plot(tt, 'bx')
# plt.plot(tt, 'r-')
# plt.grid(color='g', linestyle='--', linewidth=1)

# xlim = (-1, 61)
# ylim = (-10, 10)
# plt.figure(111, figsize=(10, 10))
# plt.rcParams.update({'font.size': 6})
# ax = plt.subplot(111)
# plt.title('Antenna array RG radar')
# plt.plot(tx_x, tx_y, 'bo')
# plt.plot(rx_x, rx_y, 'mo')
# af.applyPlotStyle(ax, xlim, ylim)
# plt.grid("on")


data_azm_sf = np.concatenate((Xtb_azm, Xtg_azm, Xtm_azm, Xtu_azm), axis=2)
data_azm_sf_real = data_azm_sf[:, :, :, ::2]
data_azm_sf_imag = data_azm_sf[:, :, :, 1::2]
data_azm_sf_complex = data_azm_sf_real + 1j*data_azm_sf_imag

data_azm_sf_complex_dst = \
    open('data_azm_sf_complex_dst.bin', 'wb')
# data_azm_sf_complex.tofile(data_azm_sf_complex_dst)
data_azm_sf.tofile(data_azm_sf_complex_dst)
data_azm_sf_complex_dst.close()
###############################################################################

data_elv_sf = np.concatenate((Xtb_elv, Xtg_elv, Xtm_elv, Xtu_elv), axis=2)
data_elv_sf_real = data_elv_sf[:, :, :, ::2]
data_elv_sf_imag = data_elv_sf[:, :, :, 1::2]
data_elv_sf_complex = data_elv_sf_real + 1j*data_elv_sf_imag

data_elv_sf_complex_dst = \
    open('data_elv_sf_complex_dst.bin', 'wb')
# data_elv_sf_complex.tofile(data_elv_sf_complex_dst)
data_elv_sf.tofile(data_elv_sf_complex_dst)
data_elv_sf_complex_dst.close()
###############################################################################

data_vel_sf = np.concatenate((Xtb_vel, Xtg_vel, Xtm_vel, Xtu_vel), axis=2)
data_vel_sf_real = data_vel_sf[:, :, :, ::2]
data_vel_sf_imag = data_vel_sf[:, :, :, 1::2]
data_vel_sf_complex = data_vel_sf_real + 1j*data_vel_sf_imag

data_vel_sf_complex_dst = \
    open('data_vel_sf_complex_dst.bin', 'wb')
# data_vel_sf_complex.tofile(data_vel_sf_complex_dst)
data_vel_sf.tofile(data_vel_sf_complex_dst)
data_vel_sf_complex_dst.close()
###############################################################################
# data_elv_sf = Xt_int[]
# data_vel_sf = Xt_int[]

#=================================================================================================

print("\n---------------------        KARON        ---------------------\n")
#=================================================================================================