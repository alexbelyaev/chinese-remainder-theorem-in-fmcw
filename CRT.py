# -*- coding: utf-8 -*-
"""
Created on Tue Dec 27 10:39:31 2022

@author: Belyaev
"""

from functools import reduce
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


def pref_sums_cfar(input_data, prms, thresh):

    # input_data = np.fft.fftshift(input_data, 0)
    num_train = prms['num_train']
    num_guard = prms['num_guard']
    fa_rate = prms['fa_rate']

    num_side = []
    num_side.append(num_train[0] + num_guard[0])
    num_side.append(num_train[1] + num_guard[1])
    # расширяем массив для обнаружения на краях
    rvacc = input_data
    num_cells = rvacc.shape

    # rvacc = np.concatenate((rvacc[:, num_cells[1] - num_side[1]::],
    #                         rvacc,
    #                         rvacc[:, 0:num_side[1]]), axis=1)
    rvacc = np.concatenate((rvacc[num_cells[0] - num_side[0]::, :],
                            rvacc,
                            rvacc[0:num_side[0], :]), axis=0)

    num_cells = rvacc.shape
    # threshold factor
    trainfactor = np.product(num_train)
    alpha = trainfactor*(fa_rate**(-1/trainfactor) - 1)
    peak_idx = []
    noise = np.array([])

    n = num_cells[0]
    m = num_cells[1]
    res_pref_sum_2d = [[0 for j in range(m+1)] for i in range(n+1)]

    for i in range(n):
        for j in range(m):
            res_pref_sum_2d[i + 1][j + 1] = res_pref_sum_2d[i][j + 1] + \
                res_pref_sum_2d[i + 1][j] - res_pref_sum_2d[i][j] + \
                rvacc[i][j]

    cfar_thresh = np.zeros([64, 256], float)

    for i in range(num_side[0], num_cells[0] - num_side[0] - 1):
        for j in range(num_side[1], num_cells[1] - num_side[1] - 1):
            if (i == 0 + num_side[0]):
                if (j == 56 + num_side[1]):
                    tmp = 0
            aperture = rvacc[i - 1:i + 2, j - 1:j + 2]
            maxarg = np.unravel_index(np.argmax(aperture),
                                      aperture.shape)
            if (maxarg != (1, 1)):
                continue
            if (rvacc[i, j] < thresh[j - num_side[1]]):
                continue

            # big sum
            lx = i - num_side[0]
            ly = j - num_side[1]
            rx = i + num_side[0] + 1
            ry = j + num_side[1] + 1
            big_sum = res_pref_sum_2d[rx][ry] - res_pref_sum_2d[lx][ry] - \
                res_pref_sum_2d[rx][ly] + res_pref_sum_2d[lx][ly]
            # small sum
            lx = i - num_guard[0]
            ly = j - num_guard[1]
            rx = i + num_guard[0] + 1
            ry = j + num_guard[1] + 1
            small_sum = res_pref_sum_2d[rx][ry] - res_pref_sum_2d[lx][ry] - \
                res_pref_sum_2d[rx][ly] + res_pref_sum_2d[lx][ly]
            p_noise = (big_sum - small_sum) / trainfactor
            threshold = alpha * p_noise
            cfar_thresh[i - num_side[0], j - num_side[1]] = threshold

            if rvacc[i, j] > threshold:
                peak_idx.append(np.array([i, j]))
                noise = np.append(noise, p_noise)

    peak_idx = np.array(peak_idx, dtype=int)

    if peak_idx.any():
        pntsRange = peak_idx[:, 1]
        pntsVel = peak_idx[:, 0] - num_side[0]
    else:
        print("peak_idx is empty")
        pntsRange = 0
        pntsVel = 0

    rdTrgts = (pntsVel, pntsRange)
    return rdTrgts, cfar_thresh


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


def chinese_remainder(n, a):
    sum_ = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum_ += a_i * mul_inv(p, n_i) * p
    return sum_ % prod


###############################################################################
####################            ПАРАМЕТРЫ                ######################
###############################################################################
smp_pad = 256
fd = 18.75e6
S = 50e12
ts = smp_pad / fd
c = 3e8
ti = 5.0e-6            # idle time
tr = 20.0e-6           # ramp end time
tp = ti + tr           # pulse width

f0 = 77.0e9
smp_num = 256
fd = 18.75e6
td = 1/fd
S = 50e12
ta = 5e-6

flo = f0 + ta*S        # low frequency
fhi = f0 + (ta+ts)*S   # high frequency
fc = (flo + fhi)/2.0
lmb = c/fc

Vmax = lmb/(4*tp)
print()

pls_num_az = 59
pls_num_el = 55
pls_num_vf = 63

f_smp = np.zeros(smp_pad, float)
for j_smp in range(0, smp_pad):
    f_smp[j_smp] = j_smp/(ts)

r_mas = f_smp*c/(2*S)

false_alarm_rate_mimo = 6e-1  # 3e-4 - исходный
cfar_prms = {'num_train': (2, 2), 'num_guard': (1, 1),
             'fa_rate': false_alarm_rate_mimo}

human_amp_pref = 1e3
human_factor_pref = np.zeros(smp_pad, float)
human_factor_pref[5:] = human_amp_pref / (r_mas[5:])**2
human_factor_pref[0:5] = human_factor_pref[5]
###############################################################################
####################       АЗИМУТАЛЬНЫЙ СУБКАДР           #####################
###############################################################################
data_azm_sf_complex_dst = open('data_azm_sf_complex_dst.bin', 'rb')
# data_azm_sf_complex_dst.seek(position, 0)
Xt_azm = np.fromfile(data_azm_sf_complex_dst, dtype=np.dtype('float64'))

Xt_azm_I = Xt_azm[::2]
Xt_azm_Q = Xt_azm[1::2]
Xt_azm = Xt_azm_I + 1j*Xt_azm_Q

Xt_azm = np.reshape(Xt_azm, (pls_num_az, 4, 16, 256))

rv_azm = abs(np.fft.fftn(Xt_azm[:, 0, 0, :], (64, 256)))

plt.figure()
plt.imshow(np.fft.fftshift(rv_azm, 0))

peak_idx = pref_sums_cfar(rv_azm, cfar_prms, human_factor_pref)
peak_idx = np.array(peak_idx[0], dtype=int).T

pnts_mag_RV_azm = np.zeros([64, 256], dtype=float)
pnts_range = peak_idx[:, 1]
pnts_vel = peak_idx[:, 0]

pnts_mag_RV_azm[pnts_vel, pnts_range] = 1

plt.figure()
plt.imshow(np.fft.fftshift(pnts_mag_RV_azm, 0), cmap=plt.cm.get_cmap('hot'))
###############################################################################
####################       УГЛОМЕСТНЫЙ СУБКАДР           ######################
###############################################################################
data_elv_sf_complex_dst = open('data_elv_sf_complex_dst.bin', 'rb')
# data_azm_sf_complex_dst.seek(position, 0)
Xt_elv = np.fromfile(data_elv_sf_complex_dst, dtype=np.dtype('float64'))

Xt_elv_I = Xt_elv[::2]
Xt_elv_Q = Xt_elv[1::2]
Xt_elv = Xt_elv_I + 1j*Xt_elv_Q

Xt_elv = np.reshape(Xt_elv, (pls_num_el, 5, 16, 256))

rv_elv = abs(np.fft.fftn(Xt_elv[:, 0, 0, :], (64, 256)))

plt.figure()
plt.imshow(np.fft.fftshift(rv_elv, 0))

peak_idx = pref_sums_cfar(rv_elv, cfar_prms, human_factor_pref)
peak_idx = np.array(peak_idx[0], dtype=int).T

pnts_mag_RV_elv = np.zeros([64, 256], dtype=float)
pnts_range = peak_idx[:, 1]
pnts_vel = peak_idx[:, 0]

pnts_mag_RV_elv[pnts_vel, pnts_range] = 1

plt.figure()
plt.imshow(np.fft.fftshift(pnts_mag_RV_elv, 0), cmap=plt.cm.get_cmap('hot'))
###############################################################################
####################        СКОРОСТНОЙ СУБКАДР           ######################
###############################################################################
data_vel_sf_complex_dst = open('data_vel_sf_complex_dst.bin', 'rb')
# data_azm_sf_complex_dst.seek(position, 0)
Xt_vel = np.fromfile(data_vel_sf_complex_dst, dtype=np.dtype('float64'))

Xt_vel_I = Xt_vel[::2]
Xt_vel_Q = Xt_vel[1::2]
Xt_vel = Xt_vel_I + 1j*Xt_vel_Q

Xt_vel = np.reshape(Xt_vel, (pls_num_vf, 4, 16, 256))

rv_vel = abs(np.fft.fftn(Xt_vel[:, 0, 0, :], (64, 256)))

plt.figure()
plt.imshow(np.fft.fftshift(rv_vel, 0))

peak_idx = pref_sums_cfar(rv_vel, cfar_prms, human_factor_pref)
peak_idx = np.array(peak_idx[0], dtype=int).T

pnts_mag_RV_vel = np.zeros([64, 256], dtype=float)
pnts_range = peak_idx[:, 1]
pnts_vel = peak_idx[:, 0]

pnts_mag_RV_vel[pnts_vel, pnts_range] = 1

plt.figure()
plt.imshow(np.fft.fftshift(pnts_mag_RV_vel, 0), cmap=plt.cm.get_cmap('hot'))
###############################################################################

if __name__ == '__main__':
    az_vel = 59
    el_vel = 55
    vel_vel = 63
    vel_vel2 = 69
    n = [az_vel, el_vel, vel_vel]

    for i in range(1):
        for j in range(1):
            i = 28
            j = 5
            a = [i, j, 59]
            # if (chinese_remainder(n, a) < 600):
            print(i, j, chinese_remainder(n, a))
